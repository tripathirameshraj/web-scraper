<?php

namespace App\Data;

/**
 * Package data attributes
 * 
 */
class Package
{
    /** @var string Title of the package*/
    private $title;

    /** @var string Description of the package */
    private $description;

    /** @var string Price Deescription of the package */
     private $price;

     /** @var float Float Price of the package */
     private $floatPrice;

    /** @var string Discount of the package */
    private $discount;

    /**
     * Constructor
     */
    public function __construct(string $title, string $description, string $price, string $discount = '')
    {
        $this->title = $title;
        $this->description = $description;
        $this->price = $price;
        $this->discount = $discount;
        $this->setFloatPrice();
    }

    /**
     * Get the package title
     * 
     * @return string
     */
    public function getTitle() {
        return $this->title;
    }

    /**
     * Get the package description
     * 
     * @return string
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * Get the package price
     * 
     * @return string
     */
    public function getPrice() {
        return $this->price;
    }

    /**
     * set float price from string price description
     * 
     * @return void
     */
    private function setFloatPrice(){
        
        $this->floatPrice = (float) preg_replace( '/[^0-9\.]/', '', $this->price);
    }

    /**
     * get float price from string price
     * 
     * @return float
     */
    public function getFloatPrice(){

        return $this->floatPrice;
    }

    /**
     * Get the package discount
     * 
     * @return string
     */
    public function getDiscount() {
        return $this->discount;
    }

    /**
     * Get the package object into an array
     * 
     * @return array
     */
    public function toArray(){
        return [
            'title' => $this->title,
            'description' => $this->description,
            'price' => $this->price,
            'discount' => $this->discount,
        ];
    }
}