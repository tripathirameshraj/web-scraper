<?php

namespace App\Services;

use Goutte\Client;
use App\Data\Package;
use App\Services\ScraperInterface;
use Symfony\Component\HttpClient\HttpClient;

/**
 * Class to scrape packages from Videx 
 * 
 */
class VidexScraper implements ScraperInterface
{
    /** @var Client Array The web scraper */
    private $client;

    /**
     * Constructor
     */
    public function __construct(){
        $this->client = new Client(HttpClient::create(['timeout' => 60]));
    }

    /**
     * This function returns an array of all the packages after scraping the website
     * 
     * @return array
     */
    public function scrape()
    {
        // Get the page
        $crawler = $this->client->request('GET', 'https://videx.comesconnected.com/');

        $packages = [];

        // filter through html elements to get all available packages
        $packages = $crawler->filter('.package')->each(function ($node) {

            $title = $node->filter('h3')->text();
            $description = $node->filter('.package-name')->text();
            $priceWithDiscount = $node->filter('.package-price')->text();

            $discountNode = $node->filter('.package-price p');
            if($discountNode->count()){
                $discount = $discountNode->text();
                $price = trim(str_replace($discount, '', $priceWithDiscount));
            } else {
                $discount = '';
                $price = $priceWithDiscount;
            };

            // Create a package
            return new Package($title, $description, $price, $discount);   
        });

        // Order all packages by price
        usort($packages, function($pkg1, $pkg2) {
            return $pkg1->getFloatPrice() < $pkg2->getFloatPrice();
        });

        // Get an array of all packages
        $packagesArr = [];
        foreach ($packages as $pkg) {
            $packagesArr[] = $pkg->toArray();
        }
       // file_put_contents('output', print_r($packagesArr, true));
        return $packagesArr;
    }
}