<?php

namespace App\Services;
/**
 * Scraper interface
 * 
 */
interface ScraperInterface
{
    public function scrape();
}