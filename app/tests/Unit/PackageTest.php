<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Data\Package;


class PackageTest extends TestCase
{
    /**
     * Can create a package
     *
     * @dataProvider packageAttributesProvider
     */
    public function testCanCreatePackage(string $title, string $description, string $price, string $discount, array $expected)
    {
        $package = new Package($title, $description, $price, $discount);
        $packageArr = $package->toArray();
        $this->assertSame($expected, $packageArr);
        return $package;
    }

    /**
     * Can get package title
     *
     */
    public function testCanGetTitle()
    {
        $package = new package('title', '', 'price 45', '8 discount');
        $title = $package->getTitle();
        $this->assertEquals($title, 'title');
    }

    /**
     * Can get package description
     *
     * @return void
     */
    public function testCanGetDescription()
    {
        $package = new package('title', 'description', '£80.00 price', '£8 discount');
        $description = $package->getDescription();
        $this->assertEquals($description, 'description');
    }

    /**
     * Can get package price in description
     *
     * @return void
     */
    public function testCanGetPrice()
    {
        $package = new package('title', 'description', '£70 price', '8 discount');
        $price = $package->getPrice();
        $this->assertEquals($price, '£70 price');
    }

    /**
     * Can get package price in float
     *
     * @return void
     */
    public function testCanGetFloatPrice()
    {
        $package = new package('title', 'description', '£35.00', '');
        $floatPrice = $package->getFloatPrice();
        $this->assertEquals($floatPrice, '35');
    }

    /**
     * Can get the discount information on the package
     *
     * @return void
     */
    public function testCanGetDiscount()
    {
        $package = new package('title', 'description', '£35.00', 'Save £5');
        $discount = $package->getDiscount();
        $this->assertEquals($discount, 'Save £5');
    }

    /**
     * Can get empty discount information on the package
     *
     * @return void
     */
    public function testCanGetEmptyDiscount()
    {
        $package = new package('title', 'description', '£35.00');
        $discount = $package->getDiscount();
        $this->assertEquals($discount, '');
    }

    /**
     * Data providers for testCanCreatePackage
     *
     */
    public function packageAttributesProvider(): array
    {
        return [
            'null data'  => ['', '', '','',['title' => '',
            'description' => '',
            'price' => '',
            'discount' => '']],
            'null discount' => ['title', 'description', '£45.00 (inc. VAT) Per Year','', ['title' => 'title',
                                                                                            'description' => 'description',
                                                                                            'price' => '£45.00 (inc. VAT) Per Year',
                                                                                            'discount' => '']],
            'some discount' => ['title', 'description', '£50.00 (inc. VAT) Per Year','Save £5',['title' => 'title',
                                                                                            'description' => 'description',
                                                                                            'price' => '£50.00 (inc. VAT) Per Year',
                                                                                            'discount' => 'Save £5']]
        ];
    }
}