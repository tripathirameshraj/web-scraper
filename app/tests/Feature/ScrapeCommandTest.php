<?php

namespace Tests\Feature;

use Tests\TestCase;
use Artisan;

/**
 * Class to test Scrape command
 * 
 */
class ScrapeCommandTest extends TestCase
{
    
    /**
     * Test the scrape command
     *
     * @return void
     */
    public function testCanScrape()
    {
        Artisan::call('scrape');
        $result = Artisan::output();
        $this->assertJson($result);
        $resultArr = json_decode($result, true);
        $this->assertIsArray($resultArr);
        foreach($resultArr as $pkg){
          $pkgKeys = array_keys($pkg);
          sort($pkgKeys);
          $expectedPkgKeys  = ['title', 'description', 'price', 'discount'];
          sort($expectedPkgKeys);
          //asserting if the result contains all the expected keys
          $this->assertEquals($pkgKeys, $expectedPkgKeys);
        }
    }
}
