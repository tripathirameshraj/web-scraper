# Introduction #

It is a simple console application which scrapes the website https://videx.comesconnected.com/ and returns a JSON array of all the package options on the page. 

### How do I get set up? ###

1. In root of the project you should find a couple of scripts
        
        *   deploy.sh - will bring up the environments
        *   runTest.sh - will run unit tests

       
2. Run the below commands to scrape the site and get JSON array of all the package 
        * docker exec php-apache-container php webScraper scrape
        

### Fire it up ###

* run ./deploy.sh
